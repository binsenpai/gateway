import WebSocket from "ws";
import { Redis } from "ioredis";
import { logger } from "./utils/Logger";
import { OpCode } from "./Constants";
import { Guild, Payload, ReadyEvent } from "./Payload";
import { cache } from "./LevelDBCache";
import Timeout = NodeJS.Timeout;
import { DistributedShardQueue } from "./DistributedShardQueue";

export class Shard {
    private readonly redis: Redis;
    private readonly queue: DistributedShardQueue;
    private readonly token: string;
    private readonly max: number;
    public readonly id: number;

    private socket: WebSocket;
    private heartbeatTimer: Timeout;
    private sessionId: string;
    private seq: number;

    constructor(redis: Redis, queue: DistributedShardQueue, token: string, id: number, max: number) {
        this.redis = redis;
        this.queue = queue;
        this.token = token;
        this.id = id;
        this.max = max;
    }

    public async start() {
        logger.info(`Starting shard ${this.id}/${this.max}...`);
        let seq = await this.redis.get(`shard:${this.id}:seq`);
        if (seq) {
            this.seq = parseInt(seq);
        }
        this.sessionId = await this.redis.get(`shard:${this.id}:session_id`);
        //console.log(this.seq, this.sessionId);

        this.socket = new WebSocket("wss://gateway.discord.gg/?v=6&encoding=json");
        this.socket.on("message", this.onMessage.bind(this));
        this.socket.on("close", this.onClose.bind(this));
    }

    public async checkSession(): Promise<boolean> {
        this.sessionId = await this.redis.get(`shard:${this.id}:session_id`);
        if (this.sessionId != null) {
            let heartbeat = parseInt(await this.redis.get(`shard:${this.id}:last_heartbeat`));
            return !isNaN(heartbeat) && (heartbeat + 60000) > Date.now();
        }
        return false;
    }

    public async clearSession() {
        await this.redis.del(`shard:${this.id}:session_id`, `shard:${this.id}:seq`, `shard:${this.id}:last_heartbeat`);
    }

    private send(data: Payload) {
        //console.log("<-", data);
        this.socket.send(JSON.stringify(data));
    }

    private onClose(code: number) {
        logger.info("WebSocket connection closed with code " + code);
        if (this.heartbeatTimer) {
            clearInterval(this.heartbeatTimer);
            this.heartbeatTimer = null;
            this.queue.queue(this);
        }
    }

    private async handleDispatch(event: string, data: any) {
        switch (event) {
            case "READY": {
                data = data as ReadyEvent;
                this.sessionId = data.session_id;
                await cache.cacheUser(data.user);
                await this.redis.set(`shard:${this.id}:session_id`, this.sessionId);
                await this.redis.set(`shard:${this.id}:last_heartbeat`, Date.now());
                logger.info("READY from shard " + this.id + " with " + data.guilds.length + " guilds.");
                //console.log(event, data);
                break;
            }
            case "RESUMED": {
                logger.info("Session resumed successfully.");
                await this.redis.set(`shard:${this.id}:last_heartbeat`, Date.now());
                break;
            }
            case "GUILD_CREATE": {
                data = data as Guild;
                if (data.large) {
                    this.send({
                        op: OpCode.REQUEST_GUILD_MEMBERS,
                        d: {
                            guild_id: data.id,
                            query: "",
                            limit: 0,
                        },
                    });
                }
                await cache.cacheGuild(data);
                break;
            }
            case "GUILD_MEMBERS_CHUNK": {
                let guild = await cache.getGuild(data.guild_id);
                if (!guild) {
                    logger.warning("Received GUILD_MEMBERS_CHUNK for uncached guild, TODO: handle this borderline case.");
                    break;
                }
                logger.debug("Got guild members chunk for " + data.guild_id);

                for (const member of data.members) {
                    if (!guild.member_ids.includes(member.user.id)) {
                        guild.member_ids.push(member.user.id);
                    }
                }

                await cache.cacheGuild(guild);

                for (const member of data.members) {
                    await cache.cacheUser(member.user);
                }
                break;
            }
        }
    }

    private async onMessage(data: string) {
        const payload = JSON.parse(data) as Payload;
        //console.log("->", payload);
        if (payload.s) {
            this.seq = payload.s;
            await this.redis.set(`shard:${this.id}:seq`, this.seq);
        }

        switch (payload.op) {
            case OpCode.DISPATCH:
                await this.handleDispatch(payload.t, payload.d);
                break;
            case OpCode.HEARTBEAT_ACK:
                await this.redis.set(`shard:${this.id}:last_heartbeat`, Date.now());
                break;
            case OpCode.INVALID_SESSION:
                logger.info("The session was invalid, requeueing...");
                this.sessionId = null;
                this.seq = null;
                await this.clearSession();
                break;
            case OpCode.HELLO:
                this.heartbeatTimer = setInterval(
                    () => this.send({ op: OpCode.HEARTBEAT, d: this.seq }),
                    payload.d.heartbeat_interval);

                if (this.sessionId) {
                    logger.info("Attempting to resume shard " + this.id);
                    this.send({
                        op: OpCode.RESUME,
                        d: {
                            token: this.token,
                            session_id: this.sessionId,
                            seq: this.seq,
                        }
                    });
                } else {
                    this.send({
                        op: OpCode.IDENTIFY,
                        d: {
                            token: this.token,
                            properties: {
                                $os: "linux",
                                $browser: "KyokoBot/gateway",
                                $device: "KyokoBot/gateway"
                            },
                            shard: [this.id, this.max],
                            large_threshold: 250,
                            presence: null
                        }
                    });
                }
                break;
        }
    }
}
