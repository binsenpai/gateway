import { cache } from "./LevelDBCache";

(async () => {
    console.log(await cache.getGuild(process.argv[2]));
})();
