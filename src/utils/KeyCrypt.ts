import crypto from "crypto";

export const encryptKey = (key: string): string => {
    const cipher = crypto.createCipher("aes-256-ctr", process.env.KP_KEY);
    const crypt = cipher.update(key, "utf8", "base64");
    return crypt + cipher.final("base64");
};

export const decryptKey = (key: string): string => {
    const decipher = crypto.createDecipher("aes-256-ctr", process.env.KP_KEY);
    const crypt = decipher.update(key, "base64", "utf8");
    return crypt + decipher.final("utf8");
};
