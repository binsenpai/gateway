import levelup, { LevelUp } from "levelup";
import { Channel, Guild, User } from "./Payload";
import { logger } from "./utils/Logger";

const rocksdb = require("rocksdb");

interface CacheGuild extends Guild {
    member_ids: string[];
    channel_ids: string[];
}

export class LevelDBCache {
    private readonly channelCache: LevelUp;
    private readonly guildCache: LevelUp;
    private readonly userCache: LevelUp;

    constructor() {
        this.channelCache = levelup(rocksdb("storage/channels"));
        this.guildCache = levelup(rocksdb("storage/guilds"));
        this.userCache = levelup(rocksdb("storage/users"));
    }

    public async buikCacheGuilds(guilds: Guild[]) {
        if (!guilds) return;

        for (const guild of guilds) {
            await this.cacheGuild(guild);
        }
    }

    public async cacheChannel(channel: Channel) {
        if (channel) {
            await this.channelCache.put(channel.id, JSON.stringify(channel));
        }
    }

    public async cacheUser(user: User) {
        if (user) {
            //logger.debug("Caching user " + user.id);
            let strippedUser = {
                id: user.id,
                username: user.username,
                discriminator: user.discriminator,
                bot: user.bot,
                avatar: user.avatar,
            };
            await this.userCache.put(user.id, strippedUser);
        }
    }

    public async cacheGuild(guild: Guild) {
        if (guild) {
            let memberIds = guild.members ? guild.members.map(m => m.user.id) : [];
            let channelIds = guild.channels ? guild.channels.map(m => m.id) : [];

            let strippedGuild: CacheGuild = {
                id: guild.id,
                name: guild.name,
                icon: guild.icon,
                splash: guild.splash,
                owner_id: guild.owner_id,
                permissions: guild.permissions,
                region: guild.region,
                afk_channel_id: guild.afk_channel_id,
                afk_timeout: guild.afk_timeout,
                embed_enabled: guild.embed_enabled,
                embed_channel_id: guild.embed_channel_id,
                verification_level: guild.verification_level,
                default_message_notifications: guild.default_message_notifications,
                explicit_content_filter: guild.explicit_content_filter,
                roles: guild.roles,
                emojis: guild.emojis,
                features: guild.features,
                mfa_level: guild.mfa_level,
                application_id: guild.application_id,
                widget_enabled: guild.widget_enabled,
                widget_channel_id: guild.widget_channel_id,
                system_channel_id: guild.system_channel_id,
                joined_at: guild.joined_at,
                large: guild.large,
                unavailable: guild.unavailable,
                voice_states: guild.voice_states,
                vanity_url_code: guild.vanity_url_code,
                description: guild.description,
                banner: guild.banner,
                premium_tier: guild.premium_tier,
                premium_subscription_count: guild.premium_subscription_count,
                member_ids: memberIds,
                channel_ids: channelIds,
            };
            await this.guildCache.put(guild.id, JSON.stringify(strippedGuild));

            if (guild.channels) {
                for (const channel of guild.channels) {
                    await this.cacheChannel(channel);
                }
            }

            if (guild.members) {
                for (const member of guild.members) {
                    await this.cacheUser(member.user);
                }
            }
        }
    }

    public async deleteGuild(id: string): Promise<CacheGuild> {
        const guild = await this.getGuild(id);
        if (guild) {
            await this.guildCache.del(id);
            return guild;
        }
        return null;
    }

    public async getGuild(id: string): Promise<CacheGuild> {
        const guild = await this.guildCache.get(id, { asBuffer: false });
        if (guild) {
            return JSON.parse(guild) as CacheGuild;
        }
        return null;
    }
}

export const cache = new LevelDBCache();
