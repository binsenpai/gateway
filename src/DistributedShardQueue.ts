import { Redis } from "ioredis";
import { Shard } from "./Shard";
import { logger } from "./utils/Logger";

export class DistributedShardQueue {
    private readonly redis: Redis;
    private shards: Shard[];
    private polling: boolean;

    constructor(redis: Redis) {
        this.redis = redis;
        this.shards = [];
        this.polling = false;
    }

    public queue(shard: Shard) {
        if (this.shards.includes(shard)) {
            logger.warning("Ignoring duplicate queue for shard " + shard.id);
            return;
        }

        this.shards.push(shard);
        this.poll();
    }

    private poll() {
        if (this.polling) return;
        this.polling = true;
        //setTimeout(this.startShard.bind(this), 5500);
        process.nextTick(this.startShard.bind(this));
    }

    private async startShard() {
        if (this.shards.length === 0) {
            this.polling = false;
            return;
        }

        let nextStart = parseInt(await this.redis.get("shard:next_start"));
        if (!isNaN(nextStart) && (nextStart > Date.now())) {
            console.log(nextStart, Date.now());
            setTimeout(this.startShard.bind(this), 5500);
            return;
        }

        await this.redis.set("shard:next_start", Date.now() + 5500);
        const shard = this.shards.shift();

        if (!(await shard.checkSession())) {
            logger.info("Invalidating session on shard " + shard.id + ", we haven't reconnected in time...");
            await shard.clearSession();
        }

        await shard.start();

        setTimeout(this.startShard.bind(this), 5500);
    }
}
